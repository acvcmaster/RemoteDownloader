using Microsoft.AspNetCore.Mvc;
using RemoteDownloader.Helpers;
using RemoteDownloader.Models;
using RemoteDownloader.Services;

namespace RemoteDownloader.Business
{
    public class QueueBusiness : IQueueBusiness
    {
        public QueueBusiness(IQueueService queueService, IResponseHelper responseHelper)
        {
            QueueService = queueService;
            ResponseHelper = responseHelper;
        }

        public IQueueService QueueService { get; }
        public IResponseHelper ResponseHelper { get; }

        public ResponseModel Enqueue([FromQuery]string url)
        {
            var result = QueueService.Enqueue(url);
            return ResponseHelper.GetResponse(result);
        }

        public ResponseModel Dequeue([FromQuery]string guid)
        {
            var result = QueueService.Dequeue(guid);
            return ResponseHelper.GetResponse(result);
        }

        public ResponseModel Status([FromQuery]string guid)
        {
            var result = QueueService.Status(guid);
            return ResponseHelper.GetResponse(result);
        }

        public ResponseModel All()
        {
            var result = QueueService.All();
            return ResponseHelper.GetResponse(result);
        }
    }
}
using RemoteDownloader.Models;

namespace RemoteDownloader.Business
{
    public interface IQueueBusiness
    {
        ResponseModel Enqueue(string url);
        ResponseModel Dequeue(string guid);
        ResponseModel Status(string guid);
        ResponseModel All();
    }
}
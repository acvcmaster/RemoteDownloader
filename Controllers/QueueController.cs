using Microsoft.AspNetCore.Mvc;
using RemoteDownloader.Business;

namespace RemoteDownloader
{
    [ApiController]
    [Route("api/[action]")]
    public class QueueController : ControllerBase
    {
        public QueueController(IQueueBusiness queueBusiness)
        {
            QueueBusiness = queueBusiness;
        }

        public IQueueBusiness QueueBusiness { get; }

        [HttpPost]
        public IActionResult Enqueue(string url) => Ok(QueueBusiness.Enqueue(url));

        [HttpPost]
        public IActionResult Dequeue(string guid) => Ok(QueueBusiness.Dequeue(guid));

        [HttpGet]
        public IActionResult Status(string guid) => Ok(QueueBusiness.Status(guid));

        [HttpGet]
        public IActionResult All() => Ok(QueueBusiness.All());
    }
}
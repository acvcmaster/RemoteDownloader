namespace RemoteDownloader.Models
{
    public class ResponseModel
    {
        public object Result { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorDetails { get; set; }
        public bool Success => ErrorMessage == null;
    }
}
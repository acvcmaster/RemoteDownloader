using System;
using System.IO;
using System.Net;
using RemoteDownloader.Services;

namespace RemoteDownloader.Models
{
    public class DownloadJob
    {
        public DownloadJob(string guid, object data)
        {
            Guid = guid;
            Data = data;
        }

        public string Guid { get; }
        public double Progress { get; set; } = 0;
        public object Data { get; set; }
        public bool Done { get; set; } = false;
        public DateTime Created { get; } = DateTime.Now;
        public DateTime DoneDate { get; set; }
        public double MillisecondsElapsed => (Done ? (DoneDate - Created) : (DateTime.Now - Created)).TotalMilliseconds;
        public bool Success { get; set; } = true;
        public string Path { get; set; }
        public string Name { get; set; }
        public long BytesReceived { get; set; } = 0;
        public double DownloadSpeed { get; set; } = 0;
        private DateTime LastUpdate = DateTime.Now;
        public bool Canceled { get; set; } = false;

        public static DownloadJob Create(IGuidService guidService, object data) => new DownloadJob(guidService.GetGuid(), data);

        public void Update(DownloadProgressChangedEventArgs eventArgs)
        {
            Progress = eventArgs.ProgressPercentage;
            var elapsedSeconds = (DateTime.Now - LastUpdate).TotalSeconds;
            var bytes = eventArgs.BytesReceived - BytesReceived;
            DownloadSpeed = bytes / elapsedSeconds;
            BytesReceived = eventArgs.BytesReceived;
            LastUpdate = DateTime.Now;
        }

        public void Complete(bool cancel = false)
        {
            Done = true;
            DoneDate = DateTime.Now;
            Success = Progress == 100;
            Canceled = cancel;

            if ((!Success || Canceled) && File.Exists(Path))
                File.Delete(Path);
        }
    }
}
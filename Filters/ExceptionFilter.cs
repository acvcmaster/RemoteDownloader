using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using RemoteDownloader.Models;

namespace RemoteDownloader.Filters
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            var exception = context.Exception;
            context.HttpContext.Response.StatusCode = 500; // Internal server error
            var response = new ResponseModel
            {
                Result = null,
                ErrorMessage = exception.Message,
                ErrorDetails = exception.StackTrace
            };
            context.Result = new JsonResult(response);
            base.OnException(context);
        }
    }
}
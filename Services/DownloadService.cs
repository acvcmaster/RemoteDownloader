using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using Microsoft.Extensions.Configuration;
using RemoteDownloader.Models;

namespace RemoteDownloader.Services
{
    public class DownloadService : IDownloadService
    {
        public DownloadService(IConfiguration configuration)
        {
            DownloadDirectory = configuration.GetValue<string>("downloadDir");
            DownloadClients = new Dictionary<string, WebClient>();
        }

        private string DownloadDirectory;
        public Dictionary<string, WebClient> DownloadClients;

        public void Download(DownloadJob job)
        {
            var data = job.Data;
            if (data is string)
            {
                var client = AcquireClient(job);
                var uri = new Uri(data as string);
                var request = HEADRequest(uri);
                using (var http = new HttpClient())
                using (var response = http.SendAsync(request).Result)
                {
                    try
                    {
                        response.EnsureSuccessStatusCode();

                        var filename = HttpUtility.UrlDecode(response.Content.Headers.ContentDisposition.FileName.Replace("\"", string.Empty));
                        job.Name = filename;
                        job.Path = Path.Combine(DownloadDirectory, filename);

                        client.DownloadProgressChanged += (sender, eventArgs) => job.Update(eventArgs);
                        client.DownloadFileCompleted += (sender, eventArgs) => job.Complete();

                        client.DownloadFileAsync(uri, job.Path);
                    }
                    catch
                    {
                        job.Success = false;
                        job.Done = true;
                        job.Progress = 100;
                    }
                }

            }
            // else if (data is ...)
        }

        public void Cancel(DownloadJob job)
        {
            var client = AcquireClient(job);
            client.CancelAsync();
            client.DownloadFileCompleted += (sender, eventArgs) => job.Complete(true);
        }

        private static HttpRequestMessage HEADRequest(Uri uri) => new HttpRequestMessage(HttpMethod.Head, uri);

        private WebClient AcquireClient(DownloadJob job)
        {
            if (!DownloadClients.ContainsKey(job.Guid))
                DownloadClients.Add(job.Guid, new WebClient());
            return DownloadClients[job.Guid];
        }
    }
}
using System;

namespace RemoteDownloader.Services
{
    public class GuidService : IGuidService
    {
        public string GetGuid() => Guid.NewGuid().ToString();
    }
}
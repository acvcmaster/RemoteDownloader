namespace RemoteDownloader.Services
{
    public interface IGuidService
    {
        string GetGuid();
    }
}
using System.Threading.Tasks;
using RemoteDownloader.Models;

namespace RemoteDownloader.Services
{
    public interface IDownloadService
    {
        void Download(DownloadJob job);
        void Cancel(DownloadJob job);
    }
}
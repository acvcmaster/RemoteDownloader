using System.Collections.Generic;
using RemoteDownloader.Models;

namespace RemoteDownloader.Services
{
    public interface IQueueService
    {
        DownloadJob Enqueue(string url);
        DownloadJob Dequeue(string guid);
        DownloadJob Status(string guid);
        IDictionary<string, DownloadJob> All();
    }
}
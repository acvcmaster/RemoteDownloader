using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RemoteDownloader.Models;

namespace RemoteDownloader.Services
{
    public class QueueService : IQueueService
    {
        public QueueService(IGuidService guidService, IDownloadService downloadService)
        {
            GuidService = guidService;
            DownloadService = downloadService;
        }

        private Dictionary<string, DownloadJob> Downloads = new Dictionary<string, DownloadJob>();
        public IGuidService GuidService { get; }
        public IDownloadService DownloadService { get; }

        public DownloadJob Enqueue(string url)
        {
            DownloadJob result = DownloadJob.Create(GuidService, url);
            Downloads.Add(result.Guid, result);
            DownloadService.Download(result);
            return result;
        }

        public DownloadJob Dequeue(string guid)
        {
            DownloadJob result = Downloads[guid];
            DownloadService.Cancel(result);
            return result;
        }

        public DownloadJob Status(string guid) => Downloads[guid];

        public IDictionary<string, DownloadJob> All() => Downloads;
    }
}
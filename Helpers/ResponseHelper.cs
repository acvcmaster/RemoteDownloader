using RemoteDownloader.Models;

namespace RemoteDownloader.Helpers
{
    public class ResponseHelper : IResponseHelper
    {
        public ResponseModel GetResponse(object result) => new ResponseModel()
        {
            Result = result,
            ErrorMessage = null,
            ErrorDetails = null
        };
    }
}
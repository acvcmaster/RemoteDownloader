using RemoteDownloader.Models;

namespace RemoteDownloader.Helpers
{
    public interface IResponseHelper
    {
        ResponseModel GetResponse(object result);
    }
}